package com.xiaoshuai.plat.service;

import com.xiaoshuai.plat.base.WXBaseDao;
import com.xiaoshuai.plat.model.WXUserInfo;
/**
 * 
 * @author 宗潇帅
 * @Title WXUserInfoService
 * @时间   2017-1-13下午1:21:15
 */
public  interface WXUserInfoService extends WXBaseDao{
	
  public abstract void save(WXUserInfo paramWXUserInfo)throws Exception;

  public abstract void insert(WXUserInfo paramWXUserInfo)throws Exception;

  public abstract WXUserInfo getByOpenId(String paramString)throws Exception;
}
