package com.xiaoshuai.plat.base;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * 基类Controller 
 * 一些参数
 * Title: BaseController	
 * @author 小帅帅丶
 * @date 2016-8-1上午10:58:26
 * @version 1.0
 */
public class BaseController {
	public Map session;
	public String openId;
	public String errMsg;
	public String jsonParam;
	public String callback;
	public HttpServletRequest request;
	public HttpServletResponse response;
	/**
	 * 每次请求都会带上
	 * @param jsonParam
	 * @param callback
	 * @param openId
	 */
	@ModelAttribute
	public void setReqAndRes( Map session,String jsonParam,String callback,String openId,HttpServletRequest request,HttpServletResponse response){
        this.jsonParam = jsonParam;
        this.callback = callback;
        this.openId = openId;
        this.session = session;
        this.request = request;
        this.response = response;
	}
	public Map getSession() {
		return session;
	}
	public void setSession(Map session) {
		this.session = session;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getJsonParam() {
		return jsonParam;
	}
	public void setJsonParam(String jsonParam) {
		this.jsonParam = jsonParam;
	}
	public String getCallback() {
		return callback;
	}
	public void setCallback(String callback) {
		this.callback = callback;
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public String getRealPath(String path) {
		return request.getSession().getServletContext().getRealPath(path);

	}
}
