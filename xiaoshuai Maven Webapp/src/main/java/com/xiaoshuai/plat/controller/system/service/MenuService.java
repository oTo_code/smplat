package com.xiaoshuai.plat.controller.system.service;
/**
 * 菜单模块接口类
 * @author 小帅帅丶
 * @Title MenuService
 * @时间   2017-2-8下午3:04:42
 */
public interface MenuService {
	public String list(String name,int start, int size, String order);
	public int count(String name, int start, int size, String order);
}
