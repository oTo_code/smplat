package com.xiaoshuai.plat.controller.pc.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiaoshuai.plat.controller.pc.service.HomePageService;
import com.xiaoshuai.plat.pojo.Menu;
import com.xiaoshuai.plat.util.MenuUtils;



/**
 * 首页controller
 * @author 小帅帅丶
 * @Title HomePageController
 * @时间   2017-2-7下午4:21:11
 */
@Controller
@RequestMapping(value="/homePage")
public class HomePageController {
	private static Logger logger = Logger.getLogger(HomePageController.class);
	
	@Autowired
	private HomePageService homePageService;
	@RequestMapping(value="/home")
	public String home(Model model){
		try {
			List<Menu> list = this.homePageService.getAllMenu();
			String menus = MenuUtils.buildMenus(list);
			model.addAttribute("menus",menus);
			logger.info("获取菜单完毕");
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("获取菜单错误"+e.getMessage());
		}
		return "/view/main/index";
	}
}
