package com.xiaoshuai.plat.controller.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sun.net.www.content.text.plain;

import com.xiaoshuai.plat.base.BaseDao;
import com.xiaoshuai.plat.controller.system.service.MenuService;
/**
 * 菜单实现类
 * @author 小帅帅丶
 * @Title MenuServiceImpl
 * @时间   2017-2-8下午3:01:36
 */
@Service("menuService")
public class MenuServiceImpl implements MenuService{
	@Autowired
	private BaseDao baseDao;
	/**
	 * @param name 
	 * @param start 开始从X读取
	 * @param size  每页大小
	 * @param order 排序字段
	 */
	@Override
	public String list(String name, int start, int size, String order) {
		String sql = "select * from menu m where 1=1 ";
		List<Object> param = new ArrayList<Object>();
		if(name!=null && name.length()>0){
			sql += " and m.name like ? ";
			param.add("%"+name+"%");
		}	
		List<Map<String, Object>> list = baseDao.listByNative(sql, param.toArray(), start, size, order);
		int count = count(name,start, size, order);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("rows", list);
		JSONObject object = JSONObject.fromObject(map);
		String result = object.toString();
		return result;
	}
	/**
	 * 根据条件统计
	 * @param name
	 * @param start
	 * @param size
	 * @param order
	 * @return
	 */
	@Override
	public int count(String name, int start, int size, String order) {
		String sql = "select count(*) from menu m where 1=1 ";
		List<Object> param = new ArrayList<Object>();
		if(name!=null && name.length()>0){
			sql += " and m.name like ? ";
			param.add("%"+name+"%");
		}	
		int count = baseDao.countByNative(sql, param.toArray());
		return count;
	}

}
