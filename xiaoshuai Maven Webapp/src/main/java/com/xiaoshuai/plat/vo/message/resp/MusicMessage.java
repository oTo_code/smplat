package com.xiaoshuai.plat.vo.message.resp;
/**
 * 音乐消息
 * @author 宗潇帅
 * @修改日期 2014-7-11下午6:18:18
 */
public class MusicMessage {
	//音乐
	private Music Music;

	public Music getMusic() {
		return Music;
	}

	public void setMusic(Music music) {
		Music = music;
	}
	
}
