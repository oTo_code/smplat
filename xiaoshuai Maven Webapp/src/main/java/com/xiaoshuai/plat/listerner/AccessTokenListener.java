package com.xiaoshuai.plat.listerner;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.xiaoshuai.plat.common.weixin.WXConstants;
import com.xiaoshuai.plat.util.weixin.WeixinUtil;

@Component("accesstoken_job")
public class AccessTokenListener
{
  private static Logger logger = Logger.getLogger(AccessTokenListener.class);

  @Scheduled(cron="0 0 */2 * * ?")
  public void getAccessToken()
  {
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    logger.info("执行的时间为：" + format.format(date));
    WeixinUtil.getAccessToken(WXConstants.appId, WXConstants.appSecret);
    JSONObject object = JSONObject.fromObject(WXConstants.ACCESS_TOKEN);
    logger.info("获取的access_token为：" + object.toString());
  }
}
