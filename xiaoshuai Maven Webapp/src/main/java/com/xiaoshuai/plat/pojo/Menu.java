package com.xiaoshuai.plat.pojo;

import java.io.Serializable;

/**
 * 菜单类
 * @author 小帅帅丶
 * @Title Menu
 * @时间 2017-2-7下午4:06:39
 */
public class Menu implements Serializable {
	private static final long serialVersionUID = -2589245519866726792L;
	private Integer id;// 主键
	private String menuname;// 菜单名称
	private String url;// 菜单地址
	private Integer parentid;// 父级ID

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMenuname() {
		return menuname;
	}

	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

}
