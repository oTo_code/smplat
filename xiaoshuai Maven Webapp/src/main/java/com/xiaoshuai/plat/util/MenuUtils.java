package com.xiaoshuai.plat.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiaoshuai.plat.pojo.Menu;
import com.xiaoshuai.plat.pojo.TreeNode;

/**
 * 菜单工具类
 * 
 * @author 小帅帅丶
 * @Title MenuUtils
 * @时间 2017-2-7下午3:55:55
 */
public class MenuUtils {
	/**
	 * 将菜单数据根据级别组装
	 * @param menuList
	 * @return
	 */
	public static String buildMenus(List<Menu> menuList) {
		if (menuList == null) {
			return null;
		}
		List<TreeNode> nodeList = new ArrayList<TreeNode>();
		for (Menu m : menuList) {
			TreeNode treeNode = new TreeNode(m.getId(), m.getMenuname(),
					m.getUrl(), m.getParentid());
			nodeList.add(treeNode);
		}
		Map<String, TreeNode> map = new HashMap<String, TreeNode>();
		for (TreeNode node : nodeList) {
			map.put(node.getId(), node);
		}
		List<TreeNode> ret = new ArrayList<TreeNode>();
		for (TreeNode node : nodeList) {
			String id = node.getId();
			String parentid = node.getParentid();
			if (parentid.compareTo(id) == 0) {
				ret.add(node);
			} else {
				TreeNode parentNode = (TreeNode) map.get(parentid);
				if (parentNode == null) {
					throw new RuntimeException(String.format("节点%s的父节点%s没找到",
							node.getId(), parentid));
				}
				parentNode.addChild(node);
			}
		}
		return createHtml(ret);
	}
	/**
	 * 创建html内容
	 * @param list
	 * @return
	 */
	private static String createHtml(List<TreeNode> list) {
		String result = "";
		String menu = "<div title=\"%s\" iconCls=\"icon-ok\"  style=\"padding:10px;\"><ul class=\"easyui-tree\" data-options='data:%s'></ul></div>";
		for (TreeNode o : list) {
			result = result+ String.format(menu,new Object[] {o.getText(),JSONBinder.buildNormalBinder().toJson(o.getChildren()) });
		}
		return result;
	}
}
